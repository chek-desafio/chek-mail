FROM node:16-alpine 

WORKDIR /app

COPY package*.json ./
COPY tsconfig.json ./

RUN npm install


EXPOSE 3001
 
COPY . .


CMD ["npm", "run", "production"]
