import { Router } from 'express';
import {
  confirmedResetPassword,
  forgotPassword,
  newTransfer,
  verifyAccount,
} from '../controllers/mailsender.controller';

const router = Router();
router.post('/verify', verifyAccount);
router.post('/forgotPassword', forgotPassword);
router.post('/confirmedResetPassword', confirmedResetPassword);
router.post('/newTransfer', newTransfer);

export default router;
