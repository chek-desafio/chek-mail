import express, { Application } from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import mailRoute from './routes/mailsender.route';
require('dotenv/config');

const app: Application = express();
const PORT: string = process.env.PORT || '';

app.use(cors());

app.use(express.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(express.static('public'));

app.use('/mail', mailRoute);

app.listen(PORT, () => {
  console.log('Listening on port ' + PORT);
});
